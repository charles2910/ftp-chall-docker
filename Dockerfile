FROM debian:latest
WORKDIR /etc
RUN useradd -ms /bin/bash hiro -p '$1$0yczA4Ba$czp0YD2hfSMW6mmJTg0Pi1'
RUN useradd -ms /bin/bash anonymous
RUN apt update && apt upgrade -y && apt install -y vsftpd
RUN rm -f vsftpd.conf
COPY vsftpd.conf vsftpd.conf
COPY hiro/* /home/hiro/
COPY anonymous/* /home/anonymous/
RUN service vsftpd restart
#CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
CMD exec /bin/bash -c "while :; do vsftpd; done"

